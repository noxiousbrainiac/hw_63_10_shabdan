import React from 'react';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import BlogHeader from "./components/BlogHeader/BlogHeader";
import HomePage from "./containers/HomePage/HomePage";
import AddPage from "./containers/AddPage/AddPage";
import ReadPost from "./containers/ReadPost/ReadPost";
import EditPost from "./containers/EditPost/EditPost";

const App = () => {
    return (
        <>
            <BrowserRouter>
                <BlogHeader/>
                <Switch>
                    <Route path="/" exact component={HomePage}/>
                    <Route path="/addpost" component={AddPage}/>
                    <Route path="/:id" exact component={ReadPost}/>
                    <Route path="/:id/edit" component={EditPost}/>
                </Switch>
            </BrowserRouter>
        </>
    );
};

export default App;