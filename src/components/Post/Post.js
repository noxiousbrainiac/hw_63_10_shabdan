import React from 'react';

const Post = ({post, history, id}) => {
    const readMore = () => {
        history.push('/' + id)
    }

    return (
        <div className="flex-column card p-2 my-2">
            <span>Created on: {post.date}</span>
            <h4>{post.title}</h4>
            <button
                className="btn btn-primary align-self-start"
                onClick={readMore}
            >
                Read more >>
            </button>
        </div>
    );
};

export default Post;