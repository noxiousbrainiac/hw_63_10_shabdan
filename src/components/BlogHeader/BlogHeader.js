import React from 'react';

const BlogHeader = () => {
    return (
        <header className="bg-body py-2">
            <div className="container d-flex justify-content-evenly">
                <a className="nav-item nav-link"  href="/">My Blog</a>
                <ul className="d-flex" style={{listStyle: "none"}}>
                    <li className="mx-2">
                        <a className="nav-item nav-link" href="/">Home</a>
                    </li>
                    <li className="mx-2">
                        <a className="nav-item nav-link" href="/addpost">Add</a>
                    </li>
                    <li className="mx-2">
                        <a className="nav-item nav-link" href="/aboutpage">About</a>
                    </li>
                    <li className="mx-2">
                        <a className="nav-item nav-link" href="/contacts">Contacts</a>
                    </li>
                </ul>
            </div>
        </header>
    );
};

export default BlogHeader;