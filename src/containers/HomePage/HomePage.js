import React, {useEffect, useState} from 'react';
import AxiosUrls from "../../axiosUrls";
import Post from "../../components/Post/Post";

const HomePage = ({history}) => {
    const [posts, setPosts] = useState({});

    const getPosts = async () => {
        const {data} = await AxiosUrls('/posts.json');
        setPosts(data)
    }

    useEffect(() => {
        getPosts();
    }, [])

    return (
        <div className="container">
            {posts ? Object.keys(posts).map(post => (
                <Post key={post} post={posts[post]} history={history} id={post}/>
            )) : <h2 className="text-center m-4">Nothing was posted</h2>}
        </div>
    );
};

export default HomePage;