import React, {useState} from 'react';
import AxiosUrls from "../../axiosUrls";
import dayjs from "dayjs";

const AddPage = ({history}) => {
    const [newPost, setNewPost] = useState({
        title: "",
        text: "",
        date: ""
    })

    const addNewPost = async e => {
        e.preventDefault();

        const post = {
            title: newPost.title,
            text: newPost.text,
            date: dayjs().format("DD.MM.YYYY HH:mm:ss")
        }

        await AxiosUrls.post("/posts.json", post);
        history.replace('/');
    }

    const onInputChange = e => {
        const {name, value} = e.target;
        setNewPost(prev => ({
            ...prev,
            [name]: value,
        }));
    }

    return (
        <div className="container">
            <h4>Add new post</h4>
            <form onSubmit={newPost.title.length > 0 ||newPost.text.length > 0 ? addNewPost : null}>
                <div className="my-2">
                    <label>Title</label>
                    <input
                        value={newPost.title}
                        name="title"
                        type="text"
                        className="form-control"
                        onChange={onInputChange}
                    />
                </div>
                <div className="my-2">
                    <label>Description</label>
                    <textarea
                        style={{resize: "none", height: "150px"}}
                        className="form-control"
                        name="text"
                        value={newPost.text}
                        onChange={onInputChange}
                    />
                </div>
                <div className="my-2">
                    <button
                        type="submit"
                        className="btn btn-primary"
                    >
                        Save
                    </button>
                </div>
            </form>
        </div>
    );
};

export default AddPage;