import React, {useEffect, useState} from 'react';
import AxiosUrls from "../../axiosUrls";
import {useHistory} from "react-router-dom";

const ReadPost = (props) => {
    const [post, setPost] = useState();
    const history = useHistory();

    const getPost = async () => {
        const {data} = await AxiosUrls.get('/posts/' + props.match.params.id + '.json');
        setPost(data);
    }

    const deletePost = async () => {
        await AxiosUrls.delete('/posts/' + props.match.params.id + '.json');
        history.push('/');
    }

    const editPost = () => {
        const params = new URLSearchParams(post)
        props.history.push(props.match.params.id + '/edit?' + params.toString());
    }

    useEffect(()=>{
        getPost();
    },[]);

    return (
        post ?
        <div className="container">
            <div className="card p-2 m-2">
                <span>Created on: {post.date}</span>
                <h3>{post.title}</h3>
                <p><b>Description: </b> {post.text}</p>
                <div className="d-flex justify-content-evenly">
                    <button
                        className="btn btn-danger"
                        onClick={deletePost}
                    >
                        Delete
                    </button>
                    <button
                        className="btn btn-warning"
                        onClick={editPost}
                    >
                        Edit
                    </button>
                </div>
            </div>
        </div> : null
    );
};

export default ReadPost;