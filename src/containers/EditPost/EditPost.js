import React, {useState} from 'react';
import AxiosUrls from "../../axiosUrls";
import {useHistory} from "react-router-dom";

const EditPost = (props) => {
    const parsePost = () => {
        const params = new URLSearchParams(props.location.search);
        return Object.fromEntries(params);
    }

    const history = useHistory();

    const [rePost, setRePost] = useState(parsePost());

    const putPost = async e => {
        e.preventDefault();
        await AxiosUrls.put('/posts/' + props.match.params.id + '.json', rePost);
        history.push('/');
    }

    const onInputChange = e => {
        const {name, value} = e.target;
        setRePost(prev => ({
            ...prev,
            [name]: value,
        }));
    }

    return (
        <div className="container">
            <h4>Edit post</h4>
            <form onSubmit={putPost}>
                <div className="my-2">
                    <label>Title</label>
                    <input
                        value={rePost.title}
                        name="title"
                        type="text"
                        className="form-control"
                        onChange={onInputChange}
                    />
                </div>
                <div className="my-2">
                    <label>Description</label>
                    <textarea
                        value={rePost.text}
                        style={{resize: "none", height: "150px"}}
                        className="form-control"
                        name="text"
                        onChange={onInputChange}
                    />
                </div>
                <div className="my-2">
                    <button
                        type="submit"
                        className="btn btn-primary"
                    >
                        Save
                    </button>
                </div>
            </form>
        </div>
    );
};

export default EditPost;