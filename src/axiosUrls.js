import axios from "axios";

const AxiosUrls = axios.create({
    baseURL: 'https://blockapp2021-default-rtdb.europe-west1.firebasedatabase.app'
})

export default AxiosUrls;